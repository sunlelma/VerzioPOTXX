# History of the Elma Level File

Hi everyone, I was a bit bored and went on an archeological dig to learn about the progression of the level file in Across and ElastoMania. There are a few interesting insights about the development process and the discovery of a sort of proto-grass polygon.

## Published Level Files ##
* Across 1.0: .txt files, version 4 (Verzio: 4) for Slippery Slope and version 5 (Verzio: 5) for all other level files. This is the only level format that supports moving objects.
* Across 1.2: .lev files, version 6 (POT06)
* Elma: .lev files, version 14 (POT14)
* Elma2: .lev files, version 35 (POT35)

## Across 1.0 Era ##

### Verzio 1 ###

The original level file format was contained in .txt files. We don't have concrete information on the very first level file, but there is a high probability that it looked like this:

```
Verzio: 1
4
0 -100 -100 200 0
0 100 -100 0 200
0 100 100 -200 0
0 -100 100 0 -200
```
![](https://i.imgur.com/uYbrrem.png)

The file starts off with the Version number 1, and then defines 4 lines. These 4 lines happen to  make a box. There is no concept of polygons, and no objects.

Each line can be read as follows:

```Inverted?, X-coordinate, Y-coordinate, X-length, Y-length```

100 units of distance is equal to 1 elma meter.
If Inverted is 1, then the orientation of the line is flipped, allowing for the creation of surfaces with ground inside:

![](https://i.imgur.com/sQyo089.png)

### Verzio 2 ###
We know exactly how the 2nd version of level files worked, and have concrete information on the format of all the remaining file versions from now on. Verzio 2 most likely added objects to the file format. It's very interesting how object movement existed from the very beginning, when it ended up being such an unpopular and seldom used feature (used only in 2 published levels, Hi Flyer and Expert System)

```
Verzio: 2
4
0 -100 -100 200 0
0 100 -100 0 200
0 100 100 -200 0
0 -100 100 0 -200
Verzio: 2
1
0 0 0 100 100
1
1
1000
500
1
```
The first number after the second Verzio is the number of objects. In this file, we only have a single object, a moving Apple.

Objects are defined as follows:
```
Inverted?, X-coordinate, Y-coordinate, X-length, Y-length
MovingObject?
ObjectType
Speed
Offset
BackAndForth?
```
* First, we draw a "line" just like we did in Verzio 1.
* Then if the object moves (1), then it will move between the two ends of the line
* The object type (0-3) is the same as in Elma: Flower, Apple, Killer, Bike
* The speed determines how fast the object moves
* The offset determines where on the line the object starts
* Back-and-forth determines whether the object goes back and forth, or teleports back to the start when it reaches the end of the line.

### Verzio 3 ###
Verzio 3 simply changed the coordinates system so that 100,000 units was equal to one elma-meter, instead of 100, because we can't use fractions and 100 units was not precise enough. So a line changed from
```
0 -100 -100 200 0
```
to
```
0 -10000000 -10000000 20000000 0
```

### Verzio 4 ###
It turns out that it's very hard to make 2 lines connect exactly together when each line is defined by a starting point and a length. In fact, even the officially released levels in Across have small errors where the lines do not connect exactly. Verzio 4 changes line information so that 2 points are used instead of a point and length

![](https://i.imgur.com/C5l2kkQ.png)

This would change
```
1 -5000000 -5000000 1000000 500000
```
to
```
1 -5000000 -5000000 -4000000 -5500000
```

This is the earliest level version that is used in Across 1.0. It is only used in the level Slippery Slope.

### Verzio 5 ###
This is the level version that is used in all other levels in Across 1.0. After Verzio 4 was released, it became very obvious that inverting a line was the same thing as swapping the two points in a line. In other words,
```
1 10 10 66 66
```
is identical to
```
0 66 66 10 10
```

So the level format was simplified so that the option to invert lines was removed:
```
Verzio: 5
4
-10000000 -10000000 10000000 -1000000
10000000 -10000000 10000000 10000000
10000000 10000000 -10000000 10000000
-10000000 10000000 -10000000 -10000000
Verzio: 5
1
0 0 10000000 10000000
1
1
1000
500
1
0*
```
There also seems to be a bug in Verzio 5 where an extra 0 got added to the end.

## Across 1.2 Era ##
Balázs Rózsa decided to change the level format completely, because there were a lot of problems with the old level format. Firstly, it was in text form and so took up a lot of memory, and secondly, it did not contain any meta data about the level, such as the level name. It was also very hard to parse the data in C++. He therefore created the POTXX format, which is used to this day in Elma 2.

By the time Across 1.2 was released, he had gone through versions POT01 all the way to POT06.

### POT01 ###
The very first level file was very basic:
```
"POT01"
Checksum (aka Integrity 1) - not verified
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
...offset to 100 bytes
NumberOfPolygons
Polygons -> size, vertices
NumberOfObjects
Objects -> point, type
```

Notably, it introduced the concept of polygons defined as a closed loop of vertices, fixing a big problem with the old Verzio format

It also significantly reduced the capability of objects which were no longer able to move.

The file also contained a checksum, composed of the sum of all the properties of the level (vertex coordinates, object coordinates and object types). While the sum at this point was not used on its own, it was used to generate an encrypted number for the TopologyCheck and LockedCheck.

If the level had topology errors, TopologyCheck was set to random a number approximately 20000-30000 more than the Checksum. If the level had no errors, the random number was approximately 10000-20000 more than the checksum.

If the level was locked, LockedCheck was set to random a number approximately 20000-30000 more than the Checksum. If the level was not locked, the random number was approximately 10000-20000 more than the checksum.

### POT02 ###
Version 2 just had a few small adjustments. The Checksum was removed, and instead a random integer was generated as a level-id. This level-id could be used to make sure replay files matched with level files.

The Topology and Locked checks were now based off the random level-id. In addition, a Shareware check was added in this version.
```
*"POT02"
*LevelID
*SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
...offset to 100 bytes
NumberOfPolygons
Polygons -> size, vertices
NumberOfObjects
Objects -> point, type
```

### POT03 ###
Balázs seemed not to like using the level-id, perhaps because that meant casting a level-id (int) to a long (integrity checks). In any case, the level-id was removed and the checksum was re-introduced. Interestingly, unlike in POT01 and POT02, the Shareware, Topology and Locked checks are not verified, meaning all POT03 levels are considered unlocked full levels with no topology errors.
```
*"POT03"
*Checksum (aka Integrity 1)
SharewareCheck (aka Integrity 2) - not verified
TopologyCheck (aka Integrity 3) - not verified
LockedCheck (aka Integrity 4) - not verified
LevelName[14]
...offset to 100 bytes
NumberOfPolygons
Polygons -> size, vertices
NumberOfObjects
Objects -> point, type
```

### POT04 ###
POT04 re-adds the level-id.
```
*"POT04"
*LevelID
Checksum (aka Integrity 1) - not verified
SharewareCheck (aka Integrity 2) - not verified
TopologyCheck (aka Integrity 3) - not verified
LockedCheck (aka Integrity 4) - not verified
LevelName[14]
...offset to 100 bytes
NumberOfPolygons
Polygons -> size, vertices
NumberOfObjects
Objects -> point, type
```

### POT05 ###
POT05 re-enables the integrity checks for Shareware, Topology, and Locked levels
```
*"POT05"
LevelID
Checksum (aka Integrity 1) - not verified
SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
...offset to 100 bytes
NumberOfPolygons
Polygons -> size, vertices
NumberOfObjects
Objects -> point, type
```

### POT06 ###
POT06 is the official Across 1.2 level version. The only difference is that it adds a verification that the Checksum is valid
```
*"POT06"
LevelID
Checksum (aka Integrity 1)
SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
...offset to 100 bytes
NumberOfPolygons
Polygons -> size, vertices
NumberOfObjects
Objects -> point, type
```

## Elma Era ##
### POT07 ###
The very first addition to the level file format is the LGR system. The level file now contains an LGR name. However, the LGR system does not seem fully implemented as we know it now as we cannot specify the ground and sky. In addition, POT07 adds support for Pictures, except it doesn't - the number of pictures MUST be 0 or else the file format is invalid.

From this version forward, the Checksum is also modified to be calculated using the X and Y coordinates of the pictures
```
*"POT07"
LevelID
Checksum (aka Integrity 1)
SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
LGR name
...offset to 100 bytes
NumberOfPolygons
Polygons -> size, vertices
NumberOfObjects
Objects -> point, type
*NumberOfPictures - MUST BE ZERO
```

### POT08 (Proto-Grass) ###
A more complete file version, unlike POT07. We see a large number of additions:
* We can now define a ground and sky texture
* We have a Proto-grass-polygon
* Pictures are now properly supported

```
*"POT08"
LevelID
Checksum (aka Integrity 1)
SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
LGR name
*ground
*sky
...offset to 100 bytes
NumberOfPolygons
*Polygons -> grass, (picturename, texturename, maskname, distance, clipping,) size, vertices
NumberOfObjects
Objects -> point, type
NumberOfPictures
*Pictures -> picturename, texturename, maskname, point, distance, clipping
```

The proto-grass polygons are very interesting! If the polygon is a "grass" polygon, then the polygon also takes 3 names of images from the lgr file, as well as 2 numbers. It's not clear what the 5 parameters are supposed to be since the feature has since been removed, but I think most likely the proto-grass polygons were actually textured polygons, and accepted a picturename, texturename, maskname, distance and clipping, just like regular pictures.

### POT09 ###
Adds a gravity parameter to objects

### POT11 ###
Adds an animation number parameter to objects

### POT12 ###
Unfortunately, the proto-grass polygons are changed into the normal grass feature that we know today.
```
*"POT12"
LevelID
Checksum (aka Integrity 1)
SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
LGR name
ground
sky
...offset to 100 bytes
NumberOfPolygons
*Polygons -> grass, size, vertices
NumberOfObjects
Objects -> point, type, gravity, animnumber
NumberOfPictures
Pictures -> picturename, texturename, maskname, point, distance, clipping
```

### POT13 ###
A lower short copy of the level-id is added for unclear reasons
```
*"POT13"
*LevelID-short
LevelID
Checksum (aka Integrity 1)
SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
LevelName[14]
LGR name
ground
sky
...offset to 100 bytes
NumberOfPolygons
Polygons -> grass, size, vertices
NumberOfObjects
Objects -> point, type, gravity, animnumber
NumberOfPictures
Pictures -> picturename, texturename, maskname, point, distance, clipping
```

### POT14 ###
The final file version released with Elma. There have been 2 small changes:
* The level name can now be up to 50 characters instead of 14.
* The level file header is now larger than 100 bytes, so we can't and don't offset to the 100th byte anymore.
```
*"POT14"
*LevelID-short
LevelID
Checksum (aka Integrity 1)
SharewareCheck (aka Integrity 2)
TopologyCheck (aka Integrity 3)
LockedCheck (aka Integrity 4)
*LevelName[50]
LGR name
ground
sky

NumberOfPolygons
Polygons -> grass, size, vertices
NumberOfObjects
Objects -> point, type, gravity, animnumber
NumberOfPictures
Pictures -> picturename, texturename, maskname, point, distance, clipping
```

## Level Times ##
Elma adds support to save the top times for singleplayer and multiplayer. In Across, external levels cannot have any Best Times. However, if you open up an Across .lev file (POT06 or any POTXX) and drive in Elma, you actually can save your time! So what is going on?

Elma adds the Best Times in an encrypted format to the end of the .lev file. Even though an empty Best Times is added to any new level, it is not actually required for the level file to be valid to support Across .lev files.

If a level file does not contain the encrypted data, or is missing the Best Times Header or Footer, the level will default to having no Best Times.

```
POTXX
LevelData
BestTimesHeader: 0x0067103A
EncryptedData
BestTimesFooter: 0x00845D52
```
